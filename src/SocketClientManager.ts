import { ClientType } from "./ClientType";
import { DefaultSocketClient } from "./DefaultSocketClient";
import { DisconnectType } from "./DisconnectType";
import { Metrics } from "./Metrics";
import { SendMode } from "./SendMode";
import { SocketClientMetadata } from "./SocketClientMetadata";
import { SubscriptionManager } from "./SubscriptionManager";
import { IPayloadHandler } from "./interfaces/IPayloadHandler";
import { ISocketClient } from "./interfaces/ISocketClient";
import logger from './logger';

let log = logger.child({ class: "SocketClientManager" });

export class SocketClientManager {
	private allClients: Map<string, ISocketClient> = new Map();

	getAllClients(): Map<string, ISocketClient> {
		return this.allClients;
	}

	createClient(type: ClientType, metadata: SocketClientMetadata, payloadHandler: IPayloadHandler): ISocketClient {
		switch (type) {
			case ClientType.Binance:
				log.info(`Creating client for ${ClientType.Binance}`)
				let client = new DefaultSocketClient(metadata, new SubscriptionManager(), payloadHandler);
				this.allClients.set(client.clientId(), client);
				return client;
			// Implement and add other cases for different client types as needed
			default:
				throw new Error("Unsupported client type");
		}
	}

	async reconnect(clientId: string) {
		const client = this.allClients.get(clientId);
		if (!client) {
			log.error(`No available client with id ${clientId}`);
			return;
		}
		Metrics.createOrGetInstance().getReconnectCounter().labels(clientId).inc();
		await client.connect();
		const subs = client.getSubscriptions();
		if (subs && subs.length > 0) {
			subs.forEach(async s => await client?.send(s, SendMode.Reconnect));
		}
	}

	async disconnect(clientId: string, type?: DisconnectType) {
		const client = this.allClients.get(clientId);
		if (!client) {
			log.error(`No available client with id ${clientId}`);
		} else {
			await client.disconnect(type);
			if (!type || type !== DisconnectType.Idle) {
				this.allClients.delete(clientId);
			}
		}
	}
}