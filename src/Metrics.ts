import { Pushgateway, register, Histogram, collectDefaultMetrics, Counter } from 'prom-client';
import logger from './logger';

let log = logger.child({ class: "Metrics" });


export class Metrics {
	private static instance: Metrics;
	private payloadProcessTimer: Histogram;
	private payloadDatabasePublishTimer: Histogram;
	private reconnectCounter: Counter;

	// Make the constructor private.
	private constructor() {
		this.payloadProcessTimer = this.initPayloadProcessTimer();
		this.payloadDatabasePublishTimer = this.initPayloadDatabasePublishTimer();
		this.reconnectCounter = this.initReconnectCounter();
	}

	// Static method to get the instance of the Metrics class.
	public static createOrGetInstance(): Metrics {
		if (!Metrics.instance) {
			log.debug('Constructing singleton instance')
			collectDefaultMetrics({
				register
			});
			Metrics.instance = new Metrics();
			log.debug('Registering metric: payload_handler_processing_duration_seconds')
			register.registerMetric(Metrics.instance.getPayloadProcessTimer());
			log.debug('Registering metric: payload_db_publisher_processing_duration_seconds')
			register.registerMetric(Metrics.instance.getPayloadDatabasePublishTimer());
			log.debug('Registering metric: client_reconnect_attempts')
			register.registerMetric(Metrics.instance.getReconnectCounter());
			setInterval(() => Metrics.pushToGateway(), 15000);
		}
		return Metrics.instance;
	}

	private static pushToGateway() {
		const pushgateway = new Pushgateway('http://pushgateway:9091', {}, register);
		pushgateway.pushAdd({ jobName: 'websocket-framework' })
			.then(() => {
				log.debug('Metrics pushed to Pushgateway');
			})
			.catch(err => {
				log.error(`Could not push metrics to Pushgateway`, err);
			});
	}

	private initPayloadProcessTimer(): Histogram {
		const histogram = new Histogram({
			name: 'payload_handler_processing_duration_seconds',
			help: 'Time taken to process a payload',
			labelNames: ['client_type'],
			buckets: [0.001, 0.007, 0.1, 0.3, 0.5, 0.7, 1, 3, 5, 7, 10] // 0.001 to 10 seconds
		});
		return histogram;
	}

	private initPayloadDatabasePublishTimer(): Histogram {
		const histogram = new Histogram({
			name: 'payload_db_publisher_processing_duration_seconds',
			help: 'Time taken to publish a payload on database',
			labelNames: ['client_type'],
			buckets: [0.001, 0.007, 0.1, 0.3, 0.5, 0.7, 1, 3, 5, 7, 10] // 0.001 to 10 seconds
		});
		return histogram;
	}

	private initReconnectCounter(): Counter {
		const counter = new Counter({
			name: 'client_reconnect_attempts',
			help: 'Number of reconnect attempts made by clients',
			labelNames: ['client_id']
		});
		return counter;
	}

	public getPayloadProcessTimer(): Histogram {
		return this.payloadProcessTimer;
	}

	public getPayloadDatabasePublishTimer(): Histogram {
		return this.payloadDatabasePublishTimer;
	}

	public getReconnectCounter(): Counter {
		return this.reconnectCounter;
	}
}
