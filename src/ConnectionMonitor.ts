import { DisconnectType } from "./DisconnectType";
import { SocketClientManager } from "./SocketClientManager";


import logger from './logger';

const log = logger.child({ class: "ConnectionMonitor" });

export class ConnectionMonitor {
	private checkInterval: number;
	private socketClientManager: SocketClientManager;
	private monitoringIntervalId?: NodeJS.Timeout;

	constructor(socketClientManager: SocketClientManager, checkInterval: number = 5000) {
		this.socketClientManager = socketClientManager;
		this.checkInterval = checkInterval;
		log.info(`Connection monitor created with default interval: ${checkInterval} ms`);
	}

	startMonitoring(): void {
		if (!this.monitoringIntervalId) {
			this.monitoringIntervalId = setInterval(() => this.checkConnections(), this.checkInterval);
		}
	}

	stopMonitoring(): void {
		if (this.monitoringIntervalId) {
			clearInterval(this.monitoringIntervalId);
			this.monitoringIntervalId = undefined;
		}
	}

	isMonitoring(): boolean {
		return !!this.monitoringIntervalId;
	}

	setCheckInterval(interval: number): void {
		this.checkInterval = interval;
	}

	checkConnections(): void {
		// Get all clients from the SocketClientManager
		const allClients = this.socketClientManager.getAllClients();
		log.debug(`${allClients.size} client(s) available`);

		// Check the status of each client
		allClients.forEach((client, clientId) => {
			log.debug(`Checking connection for client with id ${clientId}`);
			if (!client.isConnected()) {
				log.error(`Client with id ${clientId} found disconnected, trying to reconnect...`);
				// If the client is not connected, try to reconnect
				this.socketClientManager.reconnect(clientId);
			} else if (client.isIdle()) {
				log.error(`Client with id ${clientId} found to be idle, trying to reconnect...`);
				// If the client is idle, disconnect and then reconnect
				this.socketClientManager.disconnect(clientId, DisconnectType.Idle);
				this.socketClientManager.reconnect(clientId);
			}
			else {
				log.debug(`Client with id ${clientId} has live connection`);
			}
		});
	}
}
