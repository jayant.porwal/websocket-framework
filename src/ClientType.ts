// ClientType.ts

export enum ClientType {
	Deribit = "DERIBIT",
	Binance = "BINANCE"
	// Add other client types as needed
}
