import { IPublisher } from "./interfaces/IPublisher";
import logger from './logger';

let log = logger.child({ class: "ConsolePublisher" });

export class ConsolePublisher implements IPublisher {
	publish(payload: string): Promise<void> {
		return new Promise((res, rej) => {
			log.info(payload)
			res();
		});
	}
}