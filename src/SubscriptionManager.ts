export class SubscriptionManager {

	private subscriptions: Map<string, string[]> = new Map();

	addSubscription(clientId: string, channel: string) {
		let subs = this.subscriptions.get(clientId);
		if (subs) {
			subs.push(channel);
		} else {
			subs = [channel];
			this.subscriptions.set(clientId, subs);
		}
	}
	getSubscriptions(clientId: string): string[] {
		return this.subscriptions.get(clientId)!;
	}
	removeSubscriptions(clientId: string) {
		this.subscriptions.delete(clientId);
	}
}