// DeribitSocketClient.ts

import { ISocketClient } from './interfaces/ISocketClient';
import WebSocket from 'ws';
import { v4 as uuidv4 } from 'uuid';
import { IPayloadHandler } from './interfaces/IPayloadHandler';
import { SubscriptionManager } from './SubscriptionManager';
import logger from './logger';
import { SendMode } from './SendMode';
import { SocketClientMetadata } from './SocketClientMetadata';
import { DisconnectType } from './DisconnectType';

let log = logger.child({ class: "DefaultSocketClient" });

export class DefaultSocketClient implements ISocketClient {
	private ws: WebSocket | null = null;
	private url: string;
	private id: string | null = null;
	private payloadHandler: IPayloadHandler;
	private subscriptionManager: SubscriptionManager;
	private idleTimeout!: NodeJS.Timeout;
	private idleThreshold = 1000; // 1 second
	private isIdleFlag = true;

	constructor(metadata: SocketClientMetadata, subscriptionManager: SubscriptionManager, payloadHandler: IPayloadHandler) {
		this.url = metadata.endpoint;
		this.id = uuidv4();
		this.payloadHandler = payloadHandler;
		this.subscriptionManager = subscriptionManager;
		this.idleThreshold = metadata.idleThresholdMs ?? this.idleThreshold;
		log = log.child({ clientId: this.id });
		this.setupIdleTimeout();
	}

	isIdle(): boolean {
		return this.isIdleFlag;
	}

	isConnected(): boolean {
		return this.ws!.readyState == WebSocket.OPEN;
	}


	clientId(): string {
		return this.id!;
	}

	async connect(): Promise<void> {
		return new Promise((resolve, reject) => {
			this.ws = new WebSocket(this.url);

			this.ws.on('open', () => {
				log.info(`Connected successfully`);
				resolve();
			});

			this.ws.on('message', async (message: any) => {
				try {
					log.debug('Payload received')
					await this.resetIdleTimeout();
					const data = JSON.parse(message);
					this.payloadHandler.handle(JSON.stringify(data));
				} catch (error) {
					log.error('Could not handle payload', error);
				}
			})

			this.ws.on('close', () => {
				log.error(`Disconnected due to unknown reasons`);
			});

			this.ws.on('error', (err) => {
				log.error(`Error occurred`, err);
				reject(err);
			});
		});
	}

	async disconnect(type?: DisconnectType): Promise<void> {
		return new Promise((resolve, reject) => {
			if (!this.ws) {
				log.warn(`Could not disconnect, client was not connected`);
				return reject(new Error('Client found disconnected'));
			}

			this.ws.close();
			this.ws.on('close', () => {
				log.warn(`Disconnected as per user request`)
				if (!type || type !== DisconnectType.Idle) {
					log.warn(`Removing subscriptions`);
					this.subscriptionManager.removeSubscriptions(this.id!);
				}
				resolve()
			});
			this.ws.on('error', (err) => {
				log.error(`Error occurred`, err);
				reject(err);
			});
		});
	}

	async send(data: string, mode?: SendMode): Promise<void> {
		mode = mode || SendMode.Default;
		return new Promise((resolve, reject) => {
			if (!this.ws || this.ws.readyState !== WebSocket.OPEN) {
				log.error(`Could not send data, client is not connected`);
				return reject(new Error('Client found disconnected'));
			}

			this.ws.send(data, (err) => {
				if (err) {
					log.error(`Could not send data`, err);
					reject(err);
				}
				else {
					log.info(`Data successfully sent`);
					if (mode == SendMode.Default) {
						this.subscriptionManager.addSubscription(this.id!, data);
					}
					resolve();
				}
			});
		});
	}

	getSubscriptions(): string[] {
		return this.subscriptionManager.getSubscriptions(this.id!);
	}

	private setupIdleTimeout() {
		this.idleTimeout = setTimeout(() => {
			this.isIdleFlag = true;
		}, this.idleThreshold);
	}

	private async resetIdleTimeout() {
		this.isIdleFlag = false;
		clearTimeout(this.idleTimeout);
		this.setupIdleTimeout();
	}
}
