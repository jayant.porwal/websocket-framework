import { DataSource } from "typeorm";
import { ClientType } from "./ClientType";
import { ConnectionMonitor } from "./ConnectionMonitor";
import { SpotDetailPayloadHandler } from "./SpotDetailPayloadHandler";
import { Metrics } from "./Metrics";
import { SocketClientManager } from "./SocketClientManager";
import logger from './logger';
import { SpotDetail } from "./entity/SpotDetail.entity";
import { SpotDetailDatabasePublisher } from "./SpotDetailDatabasePublisher";
import * as SentryMain from "@sentry/node";
import axios from 'axios';


let log = logger.child({ class: "index" });

const HEARTBEAT_INTERVAL = 60000; // 60,000 milliseconds (60 seconds)
const HEARTBEAT_URL = 'some_url';

function sendHeartbeat() {
	axios.post(HEARTBEAT_URL)
		.then(response => {
			log.debug('Heartbeat sent successfully:', response.data);
		})
		.catch(error => {
			log.error('Error sending heartbeat:', error);
		});
}

// Start the heartbeat interval
setInterval(sendHeartbeat, HEARTBEAT_INTERVAL);


const GLITCHTIP_KEY = '1234'
const GLITCHTIP_URL = `https://${GLITCHTIP_KEY}@glitchtip.systangostudios.com/3`


SentryMain.init({
	dsn: GLITCHTIP_URL,
	integrations: [
		// enable HTTP calls tracing
		new SentryMain.Integrations.Http({ tracing: true }),
		...SentryMain.autoDiscoverNodePerformanceMonitoringIntegrations(),
	],
	tracesSampleRate: 1.0

});

// Set up the environment (URLs, intervals, etc.)
const SOCKET_URL = 'wss://stream.binance.com:9443/ws/ethusdt@trade';
const CHECK_INTERVAL = 5000;


// Configure DataSource
const AppDataSource = new DataSource({
	type: 'postgres',
	host: 'postgres',
	port: 5432,
	username: 'admin',
	password: 'admin',
	database: 'websocket_framework',
	entities: [SpotDetail], // Add all your entities here
	synchronize: true, // Set to false in production
	extra: {
		max: 20,
		idleTimeoutMillis: 30000
	}
});

try {
	throw new Error('Sample error for glitchtip');
} catch (error) {
	log.error('Custom error coming via logging integration - ', error);
}

Metrics.createOrGetInstance().getReconnectCounter().inc(0);

initializeDataSource(AppDataSource)
	.then(() => {
		log.info('Datasource has been initialized');

		const dbPublisher = new SpotDetailDatabasePublisher(AppDataSource);
		const payloadHandler = new SpotDetailPayloadHandler([dbPublisher]);
		const socketClientManager = new SocketClientManager();
		const connectionMonitor = new ConnectionMonitor(socketClientManager, CHECK_INTERVAL);

		log.info('Going to create client');
		// Create a client and start the connection monitor
		const binanceClient = socketClientManager.createClient(ClientType.Binance, { endpoint: SOCKET_URL, idleThresholdMs: 30000 }, payloadHandler);

		binanceClient.connect()
			.then(() => {
				log.info('Client connected')
				connectionMonitor.startMonitoring();
			})
			.catch(error => {
				log.error('Failed to connect', error);
			});

	})
	.catch((error) => {
		log.error('Error during Data Source initialization', error);
	});


function initializeDataSource(dataSource: DataSource, retries = 5, interval = 5000) {
	return new Promise((resolve, reject) => {
		let attempts = 0;

		function attemptConnection() {
			dataSource.initialize()
				.then(() => {
					log.info('Connected to database');
					resolve({});
				})
				.catch((error) => {
					if (attempts < retries) {
						attempts++;
						log.warn(`Attempt ${attempts} failed. Retrying in ${interval / 1000} seconds...`);
						setTimeout(attemptConnection, interval);
					} else {
						log.error('All attempts to initialize the data source failed', error);
						reject(error);
					}
				});
		}

		attemptConnection();
	});
}
