import EventEmitter from "events";

export class MessageBus extends EventEmitter {

	private static INSTANCE: MessageBus;

	private constructor() {
		super();
	}

	public static getInstance(): MessageBus {
		if (MessageBus.INSTANCE) {
			return MessageBus.INSTANCE;
		} else {
			this.INSTANCE = new MessageBus();
			return MessageBus.INSTANCE;
		}
	}
}