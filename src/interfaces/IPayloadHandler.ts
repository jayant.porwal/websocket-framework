export interface IPayloadHandler {
	handle(payload: string): void;
}