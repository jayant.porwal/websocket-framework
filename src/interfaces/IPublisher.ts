export interface IPublisher {
	publish(payload: string): Promise<void>;
}