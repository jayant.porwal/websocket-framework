import { DisconnectType } from "../DisconnectType";
import { SendMode } from "../SendMode";

export interface ISocketClient {
	isConnected(): boolean;
	isIdle(): boolean;
	connect(): Promise<void>;
	disconnect(type?: DisconnectType): Promise<void>;
	send(data: string, mode?: SendMode): Promise<void>;
	clientId(): string;
	getSubscriptions(): string[];
}