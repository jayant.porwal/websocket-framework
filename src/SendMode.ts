// SendMode.ts

export enum SendMode {
	Default = "DEFAULT",
	Reconnect = "RECONNECT"
	// Add other Send modes as needed
}
