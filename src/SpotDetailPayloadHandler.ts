import { ClientType } from './ClientType';
import { Metrics } from './Metrics';
import { ConsolePublisher } from './ConsolePublisher';
import { IPayloadHandler } from './interfaces/IPayloadHandler';
import { IPublisher } from './interfaces/IPublisher';
import logger from './logger';

let log = logger.child({ class: "DefaultPayloadHandler" });

export class SpotDetailPayloadHandler implements IPayloadHandler {
	private publishers: IPublisher[];

	constructor();

	constructor(publishers: IPublisher[]);

	constructor(publishers?: IPublisher[]) {
		if (publishers && publishers.length > 0) {
			this.publishers = publishers;
		} else {
			this.publishers = [new ConsolePublisher()];
		}
	}

	async handle(payload: string): Promise<void> {
		return new Promise((resolve, reject) => {
			try {
				const end = Metrics.createOrGetInstance().getPayloadProcessTimer().startTimer();
				const parsedPayload = JSON.parse(payload);
				log.silly(`Processing payload ${JSON.stringify(payload)}`);
				if (parsedPayload && parsedPayload.p) {
					const entity = {
						assetId: 1,
						asset: parsedPayload.s,
						exchangeId: 2,
						exchange: ClientType.Binance,
						base: parsedPayload.b,
						quote: parsedPayload.q,
						price: parsedPayload.p,
					}
					end({ client_type: ClientType.Binance });
					this.publishers.forEach(async pub => {
						await pub.publish(JSON.stringify(entity));
					});
				}
				resolve();
			} catch (error) {
				log.error("Failed to process payload:", error);
				reject(error);
			}
		});
	}

}