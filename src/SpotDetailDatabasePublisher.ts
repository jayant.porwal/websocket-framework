import { DataSource, Repository } from 'typeorm';
import { IPublisher } from './interfaces/IPublisher';
import { SpotDetail } from './entity/SpotDetail.entity';
import { ClientType } from './ClientType';
import logger from './logger';
import { Metrics } from './Metrics';
import * as Sentry from "@sentry/node";

let log = logger.child({ class: "SpotDetailDatabasePublisher" });

export class SpotDetailDatabasePublisher implements IPublisher {

	private repository: Repository<SpotDetail>;

	constructor(dataSource: DataSource) {
		this.repository = dataSource.getRepository(SpotDetail);
	}

	publish(payload: string): Promise<void> {
		return new Promise(async (resolve, reject) => {
			try {
				const end = Metrics.createOrGetInstance().getPayloadDatabasePublishTimer().startTimer();
				const parsedPayload = JSON.parse(payload);
				const payloadEntity = this.repository.create(parsedPayload);
				await this.repository.save(payloadEntity);
				end({ client_type: ClientType.Binance })
				resolve();
			} catch (error) {
				log.error("Failed to publish payload:", error);
				reject(error);
			}
		});

	}

}