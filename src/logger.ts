// src/logger.ts
import * as winston from 'winston';
import Sentry from 'winston-transport-sentry-node';

const GLITCHTIP_KEY = '1234'
const GLITCHTIP_URL = `https://${GLITCHTIP_KEY}@glitchtip.systangostudios.com/3`
const options = {
	sentry: {
		dsn: GLITCHTIP_URL,
	},
	level: 'error'
};

const logger = winston.createLogger({
	level: 'debug',
	format: winston.format.combine(
		winston.format.timestamp(),
		winston.format.prettyPrint()
	),
	defaultMeta: { service: 'websocket-framework' },
	transports: [
		new winston.transports.File({ filename: 'error.log', level: 'error' }),
		new winston.transports.File({ filename: 'combined.log' }),
	],
});

if (process.env.NODE_ENV !== 'production') {
	logger.add(new winston.transports.Console({
		format: winston.format.simple(),
	}));
}

if (process.env.NODE_ENV !== 'test') {
	logger.add(new Sentry(options));
}

export default logger;
