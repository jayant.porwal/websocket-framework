import { BaseEntity, Column, CreateDateColumn, Entity, PrimaryColumn, UpdateDateColumn } from "typeorm";

export enum SpotStatus {
	ACTIVE = "ACTIVE",
	INACTIVE = "INACTIVE"
}

@Entity("spot_detail")
export class SpotDetail extends BaseEntity {

	@PrimaryColumn("int", { nullable: false, name: "asset_id" })
	assetId!: number;

	@Column("varchar", { name: "asset" })
	asset!: string;

	@PrimaryColumn("int", { nullable: false, name: "exchange_id" })
	exchangeId!: number;

	@Column("varchar", { name: "exchange" })
	exchange!: string;

	@Column("varchar", { nullable: false })
	base!: string;

	@Column("varchar", { nullable: false })
	quote!: string;

	@Column("decimal", { nullable: false, precision: 8, scale: 2 })
	price!: number;

	@Column({
		type: "enum",
		enum: SpotStatus,
		default: SpotStatus.ACTIVE,
	})
	status!: SpotStatus;

	@CreateDateColumn({ name: "created_at" })
	createdAt!: Date;

	@UpdateDateColumn({ name: "updated_at" })
	updatedAt!: Date;
}

export const spotDetailSchema = {
	assetId: { type: "number", required: true, example: 1 },
	exchangeId: { type: "number", required: true, example: 2 },
	base: { type: "string", required: true, example: 'BTC' },
	quote: { type: "string", required: true, example: 'USD' },
	price: { type: "number", required: true, example: 1000 },
	delistTime: { type: "string", required: false, example: '2023-01-01' },
	status: { type: "string", required: true, example: SpotStatus.ACTIVE },
	createdAt: { type: "string", required: false, example: '2023-01-01' },
	updatedAt: { type: "string", required: false, example: '2023-01-01' }
};
