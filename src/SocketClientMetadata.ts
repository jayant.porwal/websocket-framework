export type SocketClientMetadata = {
	endpoint: string;
	idleThresholdMs?: number;
};