# websocket-framework
This is a websocket framework that acts as a skeleton for implementing a ETL(Extract, Transform and Load) using websockets as a base.

## <a name="building"></a>building
- `cd websocket-framework`
- `npm run build`

## running (via docker)
`index.ts` shows a simple usage of this framework.

Follow the below steps to run:
- Depends on [building](#building) step.
- `docker-compose up --build`

If you are running for the first time, you will have to configure the grafana where you will use prometheus as a data source.
- Once all the services are up, open the grafana dashboard at `http://localhost:3000`
- The default username and password are both `admin`
- Once logged in, go to `Configuration` window and select the `Data Sources` tab.
- Click on the `Add data source` button and add the http url for prometheus `http://prometheus:9090`
- Once done, click on the `Save & Test` button.
- Next click on the `+` button on the left and create a dashboard by importing `grafana_dashboard.json` from [this](https://gitlab.com/-/snippets/3617982).
- If the channels in the `index.ts` file are still valid, you should see the metrics in 15-20 seconds.

To stop, run `docker-compose down`

## design
![Alt text](design.png)