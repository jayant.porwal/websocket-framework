import { MessageBus } from '../src/MessageBus';

describe('MessageBus', () => {
	it('should provide a single instance', () => {
		const instance1 = MessageBus.getInstance();
		const instance2 = MessageBus.getInstance();
		expect(instance1).toBe(instance2);
	});

	it('should be able to emit and listen to events', done => {
		const bus = MessageBus.getInstance();
		const testData = { key: 'value' };

		bus.once('testEvent', (data) => {
			expect(data).toEqual(testData);
			done();
		});

		bus.emit('testEvent', testData);
	});
});
