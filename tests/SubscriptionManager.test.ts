import { SubscriptionManager } from '../src/SubscriptionManager';

describe('SubscriptionManager', () => {
	let subManager: SubscriptionManager;

	beforeEach(() => {
		subManager = new SubscriptionManager();
	});

	it('should allow adding a subscription for a client', () => {
		const clientId = 'client1';
		const channels = ['channel1', 'channel2'];

		subManager.addSubscription(clientId, channels[0]);
		subManager.addSubscription(clientId, channels[1]);
		const subscriptions = subManager.getSubscriptions(clientId);

		expect(subscriptions).toEqual(expect.arrayContaining(channels));
	});

	it('should allow removing subscriptions for a client', () => {
		const clientId = 'client2';

		subManager.addSubscription(clientId, 'channel1');
		subManager.removeSubscriptions(clientId);
		const subscriptions = subManager.getSubscriptions(clientId);

		expect(subscriptions).toBeUndefined();
	});

	it('should return the list of subscriptions for a client', () => {
		const clientId = 'client3';
		const channels = ['channel1', 'channel2', 'channel3'];

		subManager.addSubscription(clientId, channels[0]);
		subManager.addSubscription(clientId, channels[1]);
		subManager.addSubscription(clientId, channels[2]);
		const subscriptions = subManager.getSubscriptions(clientId);

		expect(subscriptions.length).toBe(3);
		expect(subscriptions).toEqual(expect.arrayContaining(channels));
	});

});
