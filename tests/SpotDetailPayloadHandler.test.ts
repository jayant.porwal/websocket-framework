import { SpotDetailPayloadHandler } from "../src/SpotDetailPayloadHandler";

jest.mock('../src/Metrics', () => ({
	Metrics: {
		createOrGetInstance: jest.fn().mockReturnValue({
			getPayloadProcessTimer: jest.fn().mockReturnValue({
				startTimer: jest.fn().mockReturnValue(() => { }),
			}),
		}),
	},
}));

jest.mock('../src/ConsolePublisher', () => {
	return {
		ConsolePublisher: jest.fn().mockImplementation(() => {
			return {
				publish: jest.fn(),
			};
		}),
	};
});

describe('SpotDetailPayloadHandler', () => {

	it('should use ConsolePublisher by default', () => {
		const payloadHandler = new SpotDetailPayloadHandler();

		expect(payloadHandler['publishers']).toHaveLength(1);
		expect(typeof payloadHandler['publishers'][0].publish).toBe('function');
	});

	it('should handle the payload and publish', async () => {
		const payloadHandler = new SpotDetailPayloadHandler();

		await payloadHandler.handle('{"p":"some_payload"}')

		const consolePublisher = payloadHandler['publishers'][0];
		expect(consolePublisher.publish).toHaveBeenCalledWith('{"assetId":1,"exchangeId":2,"exchange":"BINANCE","price":"some_payload"}');
	});
});