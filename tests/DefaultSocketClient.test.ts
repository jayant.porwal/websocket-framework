// Test for DefaultSocketClient implementation

import { SpotDetailPayloadHandler } from '../src/SpotDetailPayloadHandler';
import { DefaultSocketClient } from '../src/DefaultSocketClient';
import { ISocketClient } from '../src/interfaces/ISocketClient';
import { IPayloadHandler } from '../src/interfaces/IPayloadHandler';
import { SubscriptionManager } from '../src/SubscriptionManager';
import WebSocket from 'ws';
import portfinder from 'portfinder';
import { SocketClientMetadata } from '../src/SocketClientMetadata';

jest.mock('../src/Metrics', () => ({
	Metrics: {
		createOrGetInstance: jest.fn().mockReturnValue({
			getPayloadProcessTimer: jest.fn().mockReturnValue({
				startTimer: jest.fn().mockReturnValue(() => { }),
			}),
		}),
	},
}));

const IDLE_THRESHOLD = 1000;

describe('DefaultSocketClient', () => {
	let client: ISocketClient;
	let defaultPayloadHandler: IPayloadHandler;
	let subscriptionManager: SubscriptionManager;
	let mockServer: WebSocket.Server<typeof WebSocket>;
	let port: number;

	// Set up WebSocket server to mock interactions
	beforeAll((done) => {
		portfinder.getPortPromise().then((foundPort) => {
			port = foundPort;
			mockServer = new WebSocket.Server({ port: foundPort });
			mockServer.on('connection', socket => {
				socket.on('message', message => {
					// Echo the received message back to the client
					socket.send(message);
				});
			});
			mockServer.on('listening', () => done());
		});
	});

	afterAll(() => {
		mockServer.close();
	});

	beforeEach(() => {
		subscriptionManager = new SubscriptionManager();
		defaultPayloadHandler = new SpotDetailPayloadHandler();
		jest.spyOn(defaultPayloadHandler, 'handle').mockImplementation(() => { });
		const originalAddSubscription = subscriptionManager.addSubscription;
		jest.spyOn(subscriptionManager, 'addSubscription').mockImplementation((...args) => {
			return originalAddSubscription.call(subscriptionManager, ...args);
		});
		const originalGetSubscriptions = subscriptionManager.getSubscriptions;
		jest.spyOn(subscriptionManager, 'getSubscriptions').mockImplementation((...args) => {
			return originalGetSubscriptions.call(subscriptionManager, ...args);
		});
		const metadata: SocketClientMetadata = {
			endpoint: `ws://localhost:${port}`,
			idleThresholdMs: IDLE_THRESHOLD,
		}
		client = new DefaultSocketClient(metadata, subscriptionManager, defaultPayloadHandler);
	});

	it('should implement ISocketClient interface', () => {
		expect(client).toHaveProperty('connect');
		expect(client).toHaveProperty('isConnected');
		expect(client).toHaveProperty('isIdle');
		expect(client).toHaveProperty('disconnect');
		expect(client).toHaveProperty('send');
		expect(client).toHaveProperty('clientId');
		expect(client).toHaveProperty('getSubscriptions');
	});

	it('should connect to WebSocket server', async () => {
		await expect(client.connect()).resolves.not.toThrow();
		// Additional assertions can be made to check if the client is in a connected state
	});

	it('should send a message to WebSocket server', async () => {
		const message = JSON.stringify({ action: 'test' });
		await client.connect(); // Ensure the client is connected before sending
		await expect(client.send(message)).resolves.not.toThrow();
		expect(subscriptionManager.addSubscription)
			.toHaveBeenCalledTimes(1);
		// Additional assertions can be made to check if the message was sent successfully
	});

	it('should be able to send multiple messages to WebSocket server', async () => {
		const message = JSON.stringify({ action: 'test' });
		await client.connect(); // Ensure the client is connected before sending
		await expect(client.send(message)).resolves.not.toThrow();
		await expect(client.send(message)).resolves.not.toThrow();
		expect(subscriptionManager.addSubscription)
			.toHaveBeenCalledTimes(2);
		expect(client.getSubscriptions()).toHaveLength(2);
		// Additional assertions can be made to check if the message was sent successfully
	});

	it('should disconnect from WebSocket server', async () => {
		await client.connect(); // Connect first
		await expect(client.disconnect()).resolves.not.toThrow();
		// Additional assertions can be made to check if the client is in a disconnected state
	});

	it('should receive a message from WebSocket server', async () => {

		const message = JSON.stringify({
			jsonrpc: '2.0',
			id: 3600,
			method: 'public/subscribe',
			params: {
				channels: [
					'deribit_price_index.btc_usd'
				]
			}
		});

		await client.connect(); // Connect first
		await client.send(message); // Send message

		setTimeout(() => { expect(defaultPayloadHandler.handle).toHaveBeenCalledWith(expect.any(String)); }, 3000);

		await new Promise(resolve => setTimeout(resolve, 3000));

		await client.disconnect();

	});

	it('should mark the client as idle if no message is received within a set interval', async () => {

		const message = '{"test": "payload"}';

		await client.connect(); // Connect first
		await client.send(message); // Send message

		await new Promise(resolve => setTimeout(resolve, IDLE_THRESHOLD + 200));
		expect(client.isIdle()).toBe(true);

		await client.send(message);
		await new Promise(resolve => setTimeout(resolve, 200)); // Let our mock echo server echo the message
		expect(client.isIdle()).toBe(false);

		await new Promise(resolve => setTimeout(resolve, IDLE_THRESHOLD / 2));
		expect(client.isIdle()).toBe(false);

		await client.disconnect();
	});
});
