import { ConnectionMonitor } from '../src/ConnectionMonitor';
import { SocketClientManager } from '../src/SocketClientManager';

describe('ConnectionMonitor', () => {
	let connMonitor: ConnectionMonitor;
	let socketClientManager: SocketClientManager;

	beforeEach(() => {
		socketClientManager = new SocketClientManager();
		connMonitor = new ConnectionMonitor(socketClientManager, 100); // using faster interval for tests
	});

	it('should start connection monitoring', () => {
		connMonitor.startMonitoring();
		expect(connMonitor.isMonitoring()).toBeTruthy();
	});

	it('should stop connection monitoring', () => {
		connMonitor.startMonitoring();
		connMonitor.stopMonitoring();
		expect(connMonitor.isMonitoring()).toBeFalsy();
	});

	it('should check connections periodically', (done) => {
		jest.spyOn(connMonitor, 'checkConnections');

		connMonitor.startMonitoring();

		setTimeout(() => {
			expect(connMonitor.checkConnections).toHaveBeenCalled();
			connMonitor.stopMonitoring();
			done();
		}, 200); // slightly more than the check interval to allow for the check to occur
	});
});
