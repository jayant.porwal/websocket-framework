import { DataSource, DataSourceOptions, Repository } from 'typeorm';
import { SpotDetailDatabasePublisher } from "../src/SpotDetailDatabasePublisher";
import { SpotDetail } from '../src/entity/SpotDetail.entity';

jest.mock('typeorm', () => {
	const actualTypeORM = jest.requireActual('typeorm');

	return {
		...actualTypeORM,
		DataSource: jest.fn().mockImplementation(() => ({
			getRepository: jest.fn(),
		})),
	};
});

jest.mock('../src/Metrics', () => ({
	Metrics: {
		createOrGetInstance: jest.fn().mockReturnValue({
			getPayloadDatabasePublishTimer: jest.fn().mockReturnValue({
				startTimer: jest.fn().mockReturnValue(() => { }),
			}),
		}),
	},
}));


describe('SpotDetailDatabasePublisher', () => {

	let mockRepository: Partial<Repository<SpotDetail>>;
	let mockDataSource: DataSource;

	beforeEach(() => {
		mockRepository = {
			create: jest.fn().mockImplementation((data) => data),
			save: jest.fn(),
		};

		// Create a mock DataSource with an empty object cast as DataSourceOptions
		const mockDataSourceOptions: DataSourceOptions = {} as DataSourceOptions;
		mockDataSource = new DataSource(mockDataSourceOptions);
		(mockDataSource.getRepository as jest.Mock).mockReturnValue(mockRepository);
	});

	it('should publish the payload to database', async () => {
		const databasePublisher = new SpotDetailDatabasePublisher(mockDataSource);
		const testPayload = '{"payload":"some_payload"}';

		await databasePublisher.publish(testPayload);

		// Check if the repository's create and save methods were called correctly
		expect(mockRepository.create).toHaveBeenCalledWith(JSON.parse(testPayload));
		expect(mockRepository.save).toHaveBeenCalledWith(JSON.parse(testPayload));
	});
});