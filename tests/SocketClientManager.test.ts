import { ClientType } from '../src/ClientType';
import { SocketClientManager } from '../src/SocketClientManager';
import { ISocketClient } from '../src/interfaces/ISocketClient';
import { DefaultSocketClient } from '../src/DefaultSocketClient';
import { SpotDetailPayloadHandler } from '../src/SpotDetailPayloadHandler';
import WebSocket from 'ws';
import portfinder from 'portfinder';
import { SocketClientMetadata } from '../src/SocketClientMetadata';
import { DisconnectType } from '../src/DisconnectType';

const IDLE_THRESHOLD = 1000;

describe('SocketClientManager', () => {
	let manager: SocketClientManager;
	let client: ISocketClient;
	let mockServer: WebSocket.Server<typeof WebSocket>;
	let port: number;


	// Set up WebSocket server to mock interactions
	beforeAll((done) => {
		portfinder.getPortPromise().then((foundPort) => {
			port = foundPort;
			mockServer = new WebSocket.Server({ port: foundPort });
			mockServer.on('connection', socket => {
				socket.on('message', message => {
					// Echo the received message back to the client
					socket.send(message);
				});
			});
			mockServer.on('listening', () => done());
		});
	});

	afterAll(() => {
		mockServer.close();
	});

	beforeEach(() => {
		manager = new SocketClientManager();
		const payloadHandler = new SpotDetailPayloadHandler();
		const metadata: SocketClientMetadata = {
			endpoint: `ws://localhost:${port}`,
			idleThresholdMs: IDLE_THRESHOLD
		}

		client = manager.createClient(ClientType.Binance, metadata, payloadHandler);
	});

	it('should create a WebSocket client', () => {

		expect(client).toBeDefined();
		expect(client).toBeInstanceOf(DefaultSocketClient);
		expect(client).toHaveProperty('connect');
		expect(client).toHaveProperty('disconnect');
		expect(client).toHaveProperty('send');
		expect(client).toHaveProperty('clientId');
	});

	it('should return list of all clients', () => {

		const allClients: Map<string, ISocketClient> = manager.getAllClients();
		expect(allClients.size).toBe(1);
	});

	it('should disconnect a client', async () => {
		const message = JSON.stringify({ action: 'test' });
		await client.connect();
		await client.send(message);
		const clientId = client.clientId();

		await manager.disconnect(clientId);

		expect(manager.getAllClients().get(clientId)).toBeUndefined();
		expect(client.getSubscriptions()).toBeUndefined();
	});


	it('should disconnect a client with disconnect type as idle', async () => {
		const message = JSON.stringify({ action: 'test' });
		await client.connect();
		await client.send(message);
		const clientId = client.clientId();

		await manager.disconnect(clientId, DisconnectType.Idle);

		expect(manager.getAllClients().get(clientId)).toBeDefined();
		expect(client.getSubscriptions()).toHaveLength(1);
	});

	it('should reconnect a client', async () => {
		// Store the original 'send' method
		const originalSend = client.send;
		// Spy on the 'send' method and call the real implementation
		jest.spyOn(client, 'send').mockImplementation(async (...args) => {
			// Call the original implementation of 'send'
			return originalSend.call(client, ...args);
		});
		const message = JSON.stringify({ action: 'test' });
		await client.connect();
		await client.send(message);
		const clientId = client.clientId();
		(client as any).ws.close();

		await manager.reconnect(clientId);

		await new Promise(resolve => setTimeout(resolve, 2000));

		expect(manager.getAllClients().get(clientId)?.isConnected()).toEqual(true);
		expect(manager.getAllClients().get(clientId)?.send).toHaveBeenCalledTimes(2);
		expect(client.getSubscriptions()).toHaveLength(1);
	}, 7000);
});
