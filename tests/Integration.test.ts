// Integration test for SocketClient infrastructure
import { SocketClientManager } from '../src/SocketClientManager';
import { ConnectionMonitor } from '../src/ConnectionMonitor';
import { IPayloadHandler } from '../src/interfaces/IPayloadHandler';
import { ClientType } from '../src/ClientType';
import WebSocket from 'ws';
import { ISocketClient } from '../src/interfaces/ISocketClient';
import portfinder from 'portfinder';
import { SocketClientMetadata } from '../src/SocketClientMetadata';

// Mock PayloadHandler to verify interaction
class MockPayloadHandler implements IPayloadHandler {
	handle = jest.fn();
}

jest.mock('../src/Metrics', () => ({
	Metrics: {
		createOrGetInstance: jest.fn().mockReturnValue({
			getPayloadProcessTimer: jest.fn().mockReturnValue({
				startTimer: jest.fn().mockReturnValue(() => { }),
			}),
			getReconnectCounter: jest.fn().mockReturnValue({
				labels: jest.fn().mockReturnValue({
					inc: jest.fn(),
				}),
			}),
		}),
	},
}));

const IDLE_THRESHOLD = 1000;

describe('SocketClient Infrastructure Integration Test', () => {
	let socketClientManager: SocketClientManager;
	let connectionMonitor: ConnectionMonitor;
	let payloadHandler: MockPayloadHandler | IPayloadHandler;
	let mockServer: WebSocket.Server<typeof WebSocket>;
	let client: ISocketClient;
	let port: number;

	// Set up WebSocket server to mock interactions
	beforeAll((done) => {
		portfinder.getPortPromise().then((foundPort) => {
			port = foundPort;
			mockServer = new WebSocket.Server({ port: foundPort });
			mockServer.on('connection', socket => {
				socket.on('message', message => {
					// Echo the received message back to the client
					socket.send(message);
				});
			});
			mockServer.on('listening', () => done());
		});
	});

	afterAll(() => {
		mockServer.close();
	});

	beforeEach(() => {
		// Instantiate the mock payload handler
		payloadHandler = new MockPayloadHandler();

		// Create the manager and the monitor
		socketClientManager = new SocketClientManager();
		connectionMonitor = new ConnectionMonitor(socketClientManager);

		const metadata: SocketClientMetadata = {
			endpoint: `ws://localhost:${port}`,
			idleThresholdMs: IDLE_THRESHOLD
		}

		// Create a new DeribitSocketClient and connect it to the mock server
		client = socketClientManager.createClient(ClientType.Binance, metadata, payloadHandler);
	});

	it('should handle payload upon receiving message', async () => {
		expect(payloadHandler.handle).not.toHaveBeenCalled();

		await client.connect();
		const messageToSend = { type: 'test', content: 'Hello World' };
		await client.send(JSON.stringify(messageToSend));

		// Since messages are asynchronous, we wait a bit to allow for message handling
		await new Promise(resolve => setTimeout(resolve, 1000));

		// Verify that the payload handler's handle method was called
		expect(payloadHandler.handle).toHaveBeenCalledWith(expect.any(String));
	});

	it('should reconnect disconnected clients', async () => {
		// We simulate a client connection
		await client.connect();

		// Disconnect the client
		(client as any).ws.close();

		// Start monitoring with a short interval for the test
		connectionMonitor.setCheckInterval(200);
		connectionMonitor.startMonitoring();

		// Wait for the monitor to kick in
		await new Promise(resolve => setTimeout(resolve, 300));

		// Verify if the client is connected again
		expect(client.isConnected()).toBeTruthy();

		connectionMonitor.stopMonitoring(); // Stop the monitoring to clean up after the test
	});

	it('should reconnect a client if it is idle', async () => {
		// Store the original 'send' method
		const originalSend = client.send;
		// Spy on the 'send' method and call the real implementation
		jest.spyOn(client, 'send').mockImplementation(async (...args) => {
			// Call the original implementation of 'send'
			return originalSend.call(client, ...args);
		});
		const message = JSON.stringify({ action: 'test' });
		await client.connect();
		// Start monitoring with a short interval for the test
		connectionMonitor.setCheckInterval(3000);
		connectionMonitor.startMonitoring();
		await client.send(message);

		await new Promise(resolve => setTimeout(resolve, 200)); // Let our mock echo server echo the message
		expect(client.isIdle()).toBe(false);

		const clientId = client.clientId();

		await new Promise(resolve => setTimeout(resolve, IDLE_THRESHOLD + 200));
		expect(client.isIdle()).toBe(true);

		await new Promise(resolve => setTimeout(resolve, IDLE_THRESHOLD * 4)); // Wait for reconnection

		expect(socketClientManager.getAllClients().get(clientId)?.isConnected()).toEqual(true);
		expect(socketClientManager.getAllClients().get(clientId)?.send).toHaveBeenCalledTimes(2);
		expect(client.getSubscriptions()).toHaveLength(1);

		connectionMonitor.stopMonitoring(); // Stop the monitoring to clean up after the test
	}, 7000);

	// Add more tests as needed to cover all integration scenarios
});
